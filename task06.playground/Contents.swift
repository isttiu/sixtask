import UIKit

//реализовать IOSCollection
struct IOSCollection{
	var num = 1
}
class Ref<T>{
	var val: T
	init(val:T){
		self.val = val
	}
}

struct Cont<T>{
	var ref: Ref<T>
	init(val:T){
		self.ref = Ref(val: val)
	}
	var val: T{
		get{
			ref.val
		}
		set{
			guard(isKnownUniquelyReferenced(&ref)) else{
				ref = Ref(val: newValue)
				return
			};ref.val = newValue
		}
	}
}
var num = IOSCollection()
var cont1 = Cont(val: num)
var cont2 = cont1

cont2.val.num = 12434


//создать протокол hotel

protocol Hotel{
	init (roomCount:Int)
}

class HotelAlfa: Hotel{
	required init(roomCount:Int){}
}

//создать протокол gamedice

protocol GameDice{
	var numberDice: Int{get}
}

extension Int: GameDice{
	var numberDice: Int{
		return diceCoub
	}
}
let diceCoub = 4
diceCoub.numberDice
print("Выпало \(diceCoub) на кубике")

//ИЛИ?
//protocol GameDice{
//
//	var numberDice: String{get}
//
//}
//
//extension Int: GameDice{
//
//	var numberDice: String{
//
//		return ("Выпало \(diceCoub) на кубике")
//
//	}
//
//}
//let diceCoub = 4
//diceCoub.numberDice


//Создать протокол с одним методом и 2 свойствами
@objc protocol Me{
	var name: String{get}
	@objc optional var age: String{get}

	func names()
}

class Iam: Me{
	var name: String
	init(name:String){
		self.name = name
	}
	func names(){
		print("я \(name)")
	}

}
let n = Iam(name: "Ира")
n.names()
//из какого видео - не знаю, не люблю задавать вопросы: видео скуторенко, видео - ссылка из второго задания
//Создать 2 протокола

protocol Platform{
	var time:Int{get}
	var code:String{get}

	func writeCode()
}

protocol Stop{
	func stopCoding()
}

class Company:Platform,Stop{
	var numberOfSpecialist: Int
	var platform: String

	init(numberofSpesialist: Int, platform: String, time: Int, code: String){

		self.numberOfSpecialist = numberofSpesialist
		self.platform = platform
		self.time = time
		self.code = code

	}
	var time: Int
	var code: String

	func writeCode() {
		print("разработка началась. \(numberOfSpecialist) человека пишут \(code) код под \(platform) ")
	}

	func stopCoding() {
		print("работа закончена через \(time) часа. сдаю тестирование")
	}
}
let p = Company(numberofSpesialist: 4, platform: "ios", time: 2, code: "какой-то")
p.writeCode()
p.stopCoding()
let g = Company(numberofSpesialist: 2, platform: "android", time: 3, code: "какой то")
g.writeCode()
g.stopCoding()
